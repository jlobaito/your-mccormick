// When you change APPName, be sure to update it in mylibs/util.js
// @see http://paulirish.com/2009/markup-based-unobtrusive-comprehensive-dom-ready-execution/
var APPNAME = {
  
  initSlider: function() {
    $('.flexslider').flexslider({
      animation: "slide"
    });
  },
  
  // Initializers
  common: {
    init: function() { 
      
    },
    finalize: function() {
      
    }
  },
  
  has_slider: {
    init: function() { 
      APPNAME.initSlider();
    },
    finalize: function() { 
      
    }
  }
};

UTIL = {
  fire: function( func,funcname, args ) {
    var namespace = APPNAME;  // indicate your obj literal namespace here
 
    funcname = ( funcname === undefined ) ? 'init' : funcname;
    if ( func !== '' && namespace[ func ] && typeof namespace[ func ][ funcname ] == 'function' ) {
      namespace[ func ][ funcname ]( args );
    }
  },
  loadEvents: function() {
    var bodyId = document.body.id;
 
    // hit up common first.
    UTIL.fire( 'common' );
 
    // do all the classes too.
    $.each( document.body.className.split( /\s+/ ), function( i, classnm ) {
      UTIL.fire( classnm );
      UTIL.fire( classnm, bodyId );
    });
    UTIL.fire( 'common', 'finalize' );
  }
};

$(document).ready(UTIL.loadEvents);

/* ================================
                  ISOTOPE
=================================== */
$('.profiles li, .grid li').hover(function() {
    $(this).find("img:last").fadeToggle();
});​



    $(".profiles li, .grid li").hover(function(e)
    {
        var randomColor = getRandomColor();
        $('.profiles li .overlay, .grid li .overlay').css('background-color', randomColor);
    });

    $(".capabilities li ").hover(function(e)
    {
        var randomColorSolid = getRandomColorSolid();
        $('.capabilities li .overlay').css('background-color', randomColorSolid);
    });


function getRandomColor()
{   var orange = "rgba(247, 152, 27, 0.7)";
    var blue = "rgba(43, 49, 129, 0.7)";
    var green = "rgba(165, 205, 56, 0.7)";
    //Store available css classes
    var classes = new Array(orange, blue, green);

    //Get a random number from 0 to 4
    var randomNumber = Math.floor(Math.random()*3);

    return classes[randomNumber];
}

function getRandomColorSolid()
{   var orange = "rgba(247, 152, 27, 1)";
    var blue = "rgba(43, 49, 129, 1)";
    var green = "rgba(165, 205, 56, 1)";
    //Store available css classes
    var classes = new Array(orange, blue, green);

    //Get a random number from 0 to 4
    var randomNumber = Math.floor(Math.random()*3);

    return classes[randomNumber];
}


$('.click').click(function(e){
  e.preventDefault();
    $('html, body').animate({
        scrollTop: $( $.attr(this, 'href') ).offset().top
    }, 800);
    return false;
});


/**
 * cbpAnimatedHeader.js v1.0.0
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2013, Codrops
 * http://www.codrops.com
 */
var cbpAnimatedHeader = (function() {

  var docElem = document.documentElement,
    header = document.querySelector( '.mainnav' ),
    $content = $( '.contentControl' ),
    didScroll = false,
    height =  $('.hero').height();
    changeHeaderOn = height;
    

  function init() {
    window.addEventListener( 'scroll', function( event ) {
      if( !didScroll ) {
        didScroll = true;
        setTimeout( scrollPage, 100 );
      }
    }, false );
  };

  function scrollPage() {
    var sys = scrollY();
    if ( sys >= changeHeaderOn ) {
      classie.add( header, 'scroll' );
      $('#nav_placeholder').delay(400).css({display: 'block'});
      
    }
    else {
      classie.remove( header, 'scroll' );
          $('#nav_placeholder').delay(400).css({display: 'none'});
    
    }
    didScroll = false;
  };
  

  function scrollY() {
    return window.pageYOffset || docElem.scrollTop;
  }

  init();

})();



$(window).scroll(function() {    
    var scroll = $(window).scrollTop(),
      header = document.querySelector( '.mainnav' );

    if (scroll >= 700) {
        $(".first").addClass("scroll");

    } else {
        $(".first").removeClass("scroll");

    }
});


/* FLEXSLIDER */
 $(window).load(function() {
    $('.flexslider').flexslider({
      slideshow: false
    });
  });


var headerHeight = $('.hero').outerHeight();
var content = $('.contentControl');
var close = $('.footnote .closer');
var open = $('.footnote .info');

content.css('margin-top', headerHeight);

close.click(function(){
  $(this).parent().animate({
    left: "-50%"
  })
});

open.click(function(){
  $(this).parent().animate({
    left:"0"
  })
})

